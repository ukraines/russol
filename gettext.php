<?php

require __dir__ . '/vendor/autoload.php';

use Google\Cloud\Speech\SpeechClient;

// Путь к ключу Json
$pathToJson = __dir__ . '/json.txt';

// Путь к исходном файлу
$fileNameFrom = __dir__ . '/audio/audio.raw';
// Путь к конечном файлу
$fileNameTo = __dir__ . '/text/transcription.txt';
$languageCode = 'en-US';
$encoding = 'LINEAR16';
$sampleRateHertz = 16000;

$speech = new SpeechClient(['keyFilePath' => $pathToJson, 'languageCode' => $languageCode, ]);

# The audio file's encoding and sample rate
$options = ['encoding' => $encoding, 'sampleRateHertz' => $sampleRateHertz, ];

# Detects speech in the audio file
$results = $speech->recognize(fopen($fileNameFrom, 'r'), $options);

if (file_exists($fileNameTo)) {
    unlink($fileNameTo);
}

if (is_array($results)) {
    foreach ($results as $result) {
        echo $result->alternatives()[0]['transcript']."<br />";
        file_put_contents($fileNameTo, $result->alternatives()[0]['transcript'] .
            PHP_EOL, FILE_APPEND);
    }
}
